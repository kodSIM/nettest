"""Client connection for network test"""

from __future__ import print_function

from twisted.internet import interfaces, reactor
from twisted.protocols.basic import LineReceiver
from twisted.internet.protocol import ClientFactory
from zope.interface import implementer

from time import time, sleep
from random import choice
from string import ascii_uppercase, ascii_lowercase, digits
from multiprocessing import Process, freeze_support, Manager

@implementer(interfaces.IPushProducer)
class Producer(object):
    """Producing transmit data for test"""

    def __init__(self, proto):
        """Initialize producer"""
        self._proto = proto
        self._count = Manager().Value("i", 0)
        self._data = "".join(choice(ascii_lowercase + ascii_uppercase + digits) for _ in range(16384))
        self._data = bytes(self._data, encoding = 'utf-8')
        self._data_len = len(self._data)
        self._paused = False
        self._exit = False
        self._startTime = 0
        self._proc_print = None

    def _printer(self, count):
        """Print transmited data each second"""
        tick = 0
        sec = 1
        while sec <= 10:
            sleep(1)
            recv = (count.value - tick)*8/1048576.0
            print("Second {:d} -- transmite - {:.2f} MB / speed - {:.2f} Mbps".format(sec, recv/8, recv))
            tick = count.value
            sec += 1


    def pauseProducing(self):
        """If a queue of sending is got"""
        self._paused = True

    def resumeProducing(self):
        """Performance of sending test data"""
        self._paused = False
        if self._startTime == 0:
            self._startTime = time()
            print("-=Start test=-")
            self._proc_print = Process(target=self._printer, args=(self._count,))
            self._proc_print.start()

        while not self._paused:
            self._proto.sendLine(self._data)
            self._count.value += self._data_len

        if (time() - self._startTime) >= 10.0:
            self._exit = True
            self._proc_print.terminate()
            self._proc_print.join()
            print("-=Finish test=-")
            print("====================================")
            print("Transmited - {:.2f} MB".format(self._count.value/1048576.0))
            print("Speed - {:.2f} Mbps".format(self._count.value*8/10485760.0))
            print("====================================")

        if self._exit == True:
            self._proto.transport.unregisterProducer()
            self._proto.transport.loseConnection()

    def stopProducing(self):
        """Sending termination"""
        self._exit = True

class EchoClient(LineReceiver):
    """Create client connection"""
    def connectionMade(self):
        producer = Producer(self)
        self.transport.registerProducer(producer, True)
        producer.resumeProducing()
  
    def connectionLost(self, reason):
        print("connection lost")

class EchoFactory(ClientFactory):
    """Manager for client connection"""
    protocol = EchoClient

    def clientConnectionFailed(self, connector, reason):
        print("Connection failed - goodbye!")
        reactor.stop()
    
    def clientConnectionLost(self, connector, reason):
        print("Connection lost - goodbye!")
        reactor.stop()


def main(ipaddr):
    f = EchoFactory()
    reactor.connectTCP(ipaddr, 8888, f)
    reactor.run()

if __name__ == '__main__':
    freeze_support()
    main("192.168.240.129")
