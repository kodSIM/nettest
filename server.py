"""Server for network test"""

from twisted.internet import reactor
from twisted.protocols.basic import LineReceiver
from twisted.internet.protocol import Factory

from time import time, sleep
from multiprocessing import Process, freeze_support, Manager

class Echo(LineReceiver):
    """ Server class"""
    def __init__(self):
        """Initialize server"""
        self._count = Manager().Value("i", 0)
        self._time = 0
        self._proc_print = None

    def _printer(self, count):
        """Print received data each second"""
        tick = 0
        sec = 1
        while sec <= 10:
            sleep(1)
            recv = (count.value - tick)*8/1048576.0
            print("Second {:d} -- received - {:.2f} MB / speed - {:.2f} Mbps".format(sec, recv/8, recv))
            tick = count.value
            sec += 1

    def connectionMade(self):
        """After client connection"""
        print("Connect from {:s}".format(self.transport.getPeer().host))
        self._time = time()
        self._proc_print = Process(target=self._printer, args=(self._count,))
        self._proc_print.start()
    
    def lineReceived(self, data):
        """calculation of the obtained data"""
        self._count.value += len(data)

    def connectionLost(self, reason):
        """Connect termination"""
        print("Disconnect")
        self._proc_print.terminate()
        self._proc_print.join()
        print("====================================")
        print("Received - {:.2f} MB".format(self._count.value/1048576.0))
        time_passed = time() - self._time
        print("Passed time - {:.2f} second".format(time_passed))
        print("Speed - {:.2f} Mbps".format(self._count.value*8/(1048576.0*time_passed)))
        print("====================================")
        self._count.value = 0


def main():
    factory = Factory()
    factory.protocol = Echo
    reactor.listenTCP(8888,factory)
    reactor.run()

if __name__ == '__main__':
    freeze_support()
    main()
