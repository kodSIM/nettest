"""Main module for test"""

from sys import argv, exit
from twisted.internet import reactor
import server
import client

def err_args():
    print("Use command: nettest [server|client] {ip}")
    exit(1)

def main (mode, ipaddr=None):
    if mode == "server":
        print("Server mode\n")
        server.main()
        exit(0)
    elif mode == "client" and ipaddr != None:
        print("Client mode\n")
        client.main(ipaddr)
        exit(0)
    else:
        err_args()

if __name__ == "__main__":
    if len(argv) == 2:
        main(argv[1])
    elif len(argv) == 3:
        main(argv[1], argv[2])
    else:
        err_args()
