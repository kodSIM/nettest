# NetTest

Simply network tester. The first experience with [Twisted framework](https://twistedmatrix.com/).

## Launch
For launch use Python3.

### Server

    python nettest.py server

### Client

    python nettest.py client [ip-address server]